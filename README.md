# A simple reverse proxy
A very simple HTTP load balancer written in Golang.

## Usage

```
$ go build
$ ./proxy
```

connect to http://localhost (default settings)

## Configuration

Edit `conf.yaml` to customize your settings.

## TODOs
```
x implement tests
- implement arguments to overwrite settings
x implement mux routing to expose /metrics endpoint 
    (currently service /metrics on hardcoded port 8080)
- reload configs dynamically
    (currently with ConfigMap the settings in the configs are refreshed, but not pick up by the proxy executable)
- tls
- create graphana dashboard and base SLO over measurements of SLI
x docker build in two stage
    (from scratch image with only proxy executable)
- implement PersistentVolumeClaim
```

## Docker build
```
docker build -t aapack.live/proxy:dev .
```

## Helm Chart
Install to k8 cluster with helm install in the rood directory
```
helm install ---name proxy proxy-chart --dry-run --debug
```

## SLI required metrics

```
The proxy should initially expose the following Prometheus metrics and labels:

inbound_request_total {dst_<X>} (counter)
inbound_request_duration_ms {dst_<X>} (counter)
inbound_response_total {dst_<X>, status_code, grpc_status_code}  (counter)
inbound_response_latency_ms {dst_<X>, status_code, grpc_status_code, le}  (histogram)
inbound_response_duration_ms {dst_<X>, status_code, grpc_status_code, le}  (histogram)
outbound_request_total {authority, src_<X>, dst_<Y>}  (counter)
outbound_request_duration_ms {authority, src_<X>, dst_<Y>}  (counter)
outbound_response_total {authority, src_<X>, dst_<Y>, status_code, grpc_status_code}  (counter)
outbound_response_latency_ms {authority, src_<X>, dst_<Y>, status_code, grpc_status_code, le}  (histogram)
outbound_response_duration_ms {authority, src_<X>, dst_<Y>, status_code, grpc_status_code, le}  (histogram)
```