package handlers

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"testing"
	"time"

	"aapack.live/proxy/models"
)

var (
	conf models.Conf
)

func TestBackendRouting(t *testing.T) {
	conf := models.Conf{
		models.Proxy{
			models.Host{"localhost", "8080"},
			[]models.Service{},
		},
	}
	msg := "Hello from backend!"
	beListen := "localhost:8090"
	beRemoteAddr := ""

	beServeMux := http.NewServeMux()
	beServeMux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Header["X-Forwarded-For"][0] != beRemoteAddr {
			t.Errorf("Expected X-Forwarded-For: %s, got: %s", r.Header["X-Forwarded-For"][0], beRemoteAddr)
		}
		fmt.Fprintf(w, msg)
	})
	beServer := &http.Server{
		Addr:    beListen,
		Handler: beServeMux,
	}

	serveMux := http.NewServeMux()
	serveMux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		beRemoteAddr, _, _ = net.SplitHostPort(r.RemoteAddr)
		next := models.Backend{"", "http://" + beListen}
		serveBackend(w, r, &next)
	})

	server := &http.Server{
		Addr:    GetServerURL(&conf),
		Handler: serveMux,
	}

	go func() {
		beServer.ListenAndServe()
	}()
	go func() {
		server.ListenAndServe()
	}()

	time.Sleep(time.Duration(100) * time.Millisecond)
	res, err := http.Get(fmt.Sprintf("http://%s/", GetServerURL(&conf)))
	if err != nil {
		log.Panic("[test] Error connecting to balancer: ", err)
	}
	bodyBytes, _ := ioutil.ReadAll(res.Body)

	if string(bodyBytes) != msg {
		t.Errorf("Expected to read %s, got: %s", msg, bodyBytes)
	}

	defer beServer.Close()
	defer server.Close()
}

func TestBackendUnavailable(t *testing.T) {
	conf := models.Conf{models.Proxy{models.Host{"localhost", "8080"}, []models.Service{}}}
	serveMux := http.NewServeMux()
	serveMux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		next := models.Backend{Url: "http://localhost:9999"}
		serveBackend(w, r, &next)
	})
	server := &http.Server{
		Addr:    GetServerURL(&conf),
		Handler: serveMux,
	}
	go func() {
		server.ListenAndServe()
	}()

	time.Sleep(time.Duration(100) * time.Millisecond)
	res, _ := http.Get(fmt.Sprintf("http://%s/", GetServerURL(&conf)))

	if res.StatusCode != 502 {
		t.Errorf("Expected status code 502, got: %d", res.StatusCode)
	}

	defer server.Close()
}
