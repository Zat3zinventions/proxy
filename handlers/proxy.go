package handlers

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"

	"aapack.live/proxy/models"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	TotalProxyRequests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "proxy_requests_total",
			Help: "Total Number of Request per Response StatusCode and site.",
		},
		[]string{"site", "statusCode"},
	)
)

type ProxyHandler struct {
	handler http.Handler
	Conf    *models.Conf
}

func (h *ProxyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	for _, service := range h.Conf.Proxy.Services {
		if r.Host == service.Domain {
			next := Next(service.Frontend.BackendStructs)
			serveBackend(w, r, &next)
		}
	}
}

func NewProxyHandler(handler http.Handler, conf *models.Conf) http.Handler {
	h := &ProxyHandler{
		handler: handler,
		Conf:    conf,
	}
	return h
}

func Next(b *models.RoundRobin) models.Backend {
	res := b.Backends[b.CurrentIndex]
	b.CurrentIndex = (b.CurrentIndex + 1) % len(b.Backends)

	return res
}

func GetServerURL(conf *models.Conf) string {
	return fmt.Sprintf("%s:%s", conf.Proxy.Listen.Address, conf.Proxy.Listen.Port)
}

func serveBackend(w http.ResponseWriter, r *http.Request, backend *models.Backend) {
	u, err := url.Parse(fmt.Sprintf("%s", backend.Url))
	if err != nil {
		log.Panic("Error parsing service Url: ", err)
	}

	log.Printf("--> %s %s", r.Method, r.URL.Path)
	proxy := httputil.NewSingleHostReverseProxy(u)
	lrw := NewLoggingResponseWriter(w)
	proxy.ServeHTTP(lrw, r)
	statusCode := lrw.statusCode
	log.Printf("Domain %s with URL %s", backend.Domain, backend.Url)
	log.Printf("<-- %d %s", statusCode, http.StatusText(statusCode))
	TotalProxyRequests.With(prometheus.Labels{"site": r.Host, "statusCode": fmt.Sprintf("%d", statusCode)}).Inc()
}

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func NewLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}
