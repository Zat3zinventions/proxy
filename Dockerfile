# build stage
FROM golang:1.12.5-alpine3.9 AS builder
LABEL Author="Nikola.Zelenkov <z@aapack.live>"
ENV DOMAIN=aapack.live 
ENV APPNAME=proxy
ENV GOBIN=/go/bin
ENV GOPATH=/go
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV WKD="${GOPATH}/src/${DOMAIN}/${APPNAME}"
WORKDIR $WKD
COPY . /tmp/${APPNAME}
RUN apk update && apk add git && \
cd /tmp/${APPNAME} && tar -cO --exclude=.dockerignore . | tar -xv -C ${WKD} && \
cd ${WKD} && mkdir -p /data/config && \
go get -d -v && go build -v && go install -v && go test -v ./... && \
rm -rf /var/cache/apk/* && rm -rf /tmp/${APPNAME}
VOLUME config /data/config
CMD ["proxy"]

# final stage
FROM scratch
WORKDIR /usr/local/bin
COPY --from=builder /go/bin/proxy .
COPY conf.yaml .
EXPOSE 80
CMD ["proxy"]