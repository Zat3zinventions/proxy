package routers

import (
	"net/http"

	"aapack.live/proxy/handlers"
	"aapack.live/proxy/models"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Route struct {
	Name    string
	Methods []string
	Pattern string
	Handler http.Handler
}

var routes []Route

func init() {
	routes = append(routes, Route{
		Name:    "MetricsHandler",
		Methods: []string{"GET"},
		Pattern: "/metrics",
		Handler: http.Handler(promhttp.Handler()),
	})
}

func NewRouter(conf *models.Conf) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	routes = append(routes, Route{
		Name:    "RootHandler",
		Methods: []string{"GET"},
		Pattern: "/",
		Handler: handlers.NewProxyHandler(&handlers.ProxyHandler{}, conf),
	})

	for _, route := range routes {
		router.
			Methods(route.Methods...).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.Handler)

		router.PathPrefix("/").Handler(handlers.NewProxyHandler(&handlers.ProxyHandler{}, conf))
	}

	return router
}
