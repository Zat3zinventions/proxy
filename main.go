package main

import (
	"net/http"

	"aapack.live/proxy/config"
	"aapack.live/proxy/handlers"
	"aapack.live/proxy/models"
	"aapack.live/proxy/routers"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	conf models.Conf
)

func init() {
	config.LoadConfig("conf", &conf)
	prometheus.MustRegister(handlers.TotalProxyRequests)
}

func main() {
	// go func() {
	// 	http.Handle("/metrics", promhttp.Handler())
	// 	log.Fatal(http.ListenAndServe(":8080", nil))
	// }()

	router := routers.NewRouter(&conf)

	// log.Fatal(http.ListenAndServe(":80", router))

	s := &http.Server{
		Addr:    handlers.GetServerURL(&conf),
		Handler: router,
		// Handler: handlers.NewProxyHandler(&handlers.ProxyHandler{}, conf),
	}

	if err := s.ListenAndServe(); err != nil {
		panic(err)
	}
}
