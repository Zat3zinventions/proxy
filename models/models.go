package models

type Conf struct {
	Proxy Proxy
}

type Proxy struct {
	Listen   Host
	Services []Service
}

type Host struct {
	Address string
	Port    string
}

type Frontend struct {
	BackendStructs *RoundRobin
}

type Backend struct {
	Domain string
	Url    string
}

type Service struct {
	Name     string
	Domain   string
	Hosts    []Host
	Frontend Frontend
}

type RoundRobin struct {
	CurrentIndex int
	Backends     []Backend
}
