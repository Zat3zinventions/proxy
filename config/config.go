package config

import (
	"fmt"
	"log"
	"strconv"

	"aapack.live/proxy/models"
	"github.com/spf13/viper"
)

func LoadConfig(config string, conf *models.Conf) {
	loadConf(config, conf)
	parseConf(conf)
}

func loadConf(configName string, conf *models.Conf) {
	viper.SetConfigType("yaml")
	viper.SetConfigName(configName)
	viper.AddConfigPath("/data/config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Error in config file: %s", err))
	}
	err = viper.Unmarshal(&conf)
	if err != nil {
		log.Fatalf("Invalid configuration file, %v", err)
	}
	if v := conf.Proxy.Listen.Port; v != "" {
		port, err := strconv.Atoi(v)
		if err != nil {
			panic(fmt.Errorf("Server port %v is not valid: %s", port, err))
		}
	} else {
		panic(fmt.Errorf("Server port is required"))
	}
	if v := conf.Proxy.Listen.Address; v == "" {
		panic(fmt.Errorf("Server bind address is required"))
	}
}

func parseConf(conf *models.Conf) {
	svc := conf.Proxy.Services
	for i, service := range svc {
		backends := make([]models.Backend, len(service.Hosts))
		for index, host := range service.Hosts {
			backends[index] = models.Backend{Domain: service.Domain, Url: fmt.Sprintf("http://%s:%s", host.Address, host.Port)}
		}
		conf.Proxy.Services[i].Frontend = models.Frontend{&models.RoundRobin{0, backends}}
	}
}
