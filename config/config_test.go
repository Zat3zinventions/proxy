package config

import (
	"fmt"
	"testing"

	"aapack.live/proxy/models"
)

var conf models.Conf

func TestLoadConfig(t *testing.T) {
	LoadConfig("config_test", &conf)
	expectedPort := "8080"
	expectedAddress := "localhost"

	if expectedPort != conf.Proxy.Listen.Port {
		t.Errorf("Port differ, expected %s got %s", expectedPort, conf.Proxy.Listen.Port)
	}
	if expectedAddress != conf.Proxy.Listen.Address {
		t.Errorf("Bind differ, expected %s got %s", expectedAddress, conf.Proxy.Listen.Address)
	}
}

func TestParseConfig(t *testing.T) {
	LoadConfig("config_test", &conf)
	parseConf(&conf)

	services := conf.Proxy.Services
	expectedServices := []models.Service{
		models.Service{
			"name.local",
			"domain.local",
			[]models.Host{models.Host{"localhost", "9090"}},
			models.Frontend{},
		},
		models.Service{
			"name.local2",
			"domain.local2",
			[]models.Host{models.Host{"localhost", "9091"}, models.Host{"localhost", "9092"}},
			models.Frontend{},
		},
	}

	if len(services) != len(expectedServices) {
		t.Errorf("Services size differ, expected: %d, got: %d", len(expectedServices), len(services))
		return
	}

	for i, service := range services {
		if service.Name != expectedServices[i].Name {
			t.Errorf("Service name %d differ, expected: %s got %s", i, expectedServices[i].Name, service.Name)
		}
		if service.Domain != expectedServices[i].Domain {
			t.Errorf("Service domain %d differ, expected: %s got %s", i, expectedServices[i].Domain, service.Domain)
		}
		for y, host := range service.Hosts {
			if host != expectedServices[i].Hosts[y] {
				t.Errorf("Service host %d differ, expected: %s got %s", y, fmt.Sprint(expectedServices[i].Hosts[y]), fmt.Sprint(host))
			}
		}
	}
}
